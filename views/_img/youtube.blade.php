@if($ytsize == 'maxresdefault')
	<div class="aspect-ratio aspect-ratio-16by9">
		<img data-src="https://img.youtube.com/vi/{{ $ytcode }}/{{ $ytsize }}.jpg"
		     width="{{ $ytw }}" height="{{ $yth }}" class="lazyload"/>
	</div>
@else
	<div class="aspect-ratio aspect-ratio-4by3">
		<img data-src="https://img.youtube.com/vi/{{ $ytcode }}/{{ $ytsize }}.jpg"
		     width="{{ $ytw }}" height="{{ $yth }}" class="lazyload"/>
	</div>
@endif