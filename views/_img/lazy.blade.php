<?php
$preventCropping = ($lzobj->prevent_cropping == 1);
$forceCropping = (isset($fc)) ? $fc : true;
$dim = _imgdim($lzw, $lzh, $preventCropping, $forceCropping);
$aspectRatio = (isset($ar)) ? $ar : '4by3';
?>
@if($forceCropping)
	<div class="aspect-ratio aspect-ratio-{{ $aspectRatio }}">
		<img data-src="{{ _cimg($lzobj->filename, $dim['w'], $dim['h'], $dim['f']) }}"
		     width="{{ $dim['w'] }}"
		     height="{{ $dim['h'] }}"
		     title="{{ $lzobj->image_title }}"
		     alt="{{ $lzobj->image_alt }}"
		     class="lazyload"/>
	</div>
@else
	<img data-src="{{ _cimg($lzobj->filename, $dim['w'], $dim['h'], $dim['f']) }}"
	     width="{{ $dim['w'] }}"
	     height="{{ $dim['h'] }}"
	     title="{{ $lzobj->image_title }}"
	     alt="{{ $lzobj->image_alt }}"
	     class="lazyload"/>
@endif