<!-- FOOTER -->
<footer class="footer">
	<div class="container">

		<div class="row">

			<!--  -->
			<div class="col-sm-3">
				<div class="widget">
					@include('larawidget', ['hook' => 'footer1', 'delay' => 0])
				</div>
			</div>
			<!--  -->

			<!-- TWITTER WIDGET -->
			<div class="col-sm-3">
				<div class="widget">
					@include('larawidget', ['hook' => 'footer2', 'delay' => 0])
				</div>
			</div>
			<!-- END TWITTER WIDGET -->

			<!-- RECENT POSTS WIDGET -->
			<div class="col-sm-3">
				<div class="widget">
					@widget('entityWidget', ['entity_key' => 'blog', 'parent' => 'footer', 'term' => null,
					'needs_image' => true, 'count' => 3, 'grid' => $data->grid])
				</div>
			</div>
			<!-- END RECENT POSTS WIDGET -->

			<!-- CONTACT WIDGET -->
			<div class="col-sm-3">
				<div class="widget">
					<h6 class="montserrat text-uppercase bottom-line">Contact Us</h6>
					@include('_partials.footer.footer_contact')
				</div>
			</div>
			<!-- END CONTACT WIDGET -->

		</div><!-- .row -->

		<div class="copyright">

			<div class="row">

				<div class="col-sm-6">
					<p class="m-0">
						&copy; {{ $settngz->company_name }}, {{ Carbon\Carbon::now()->format('Y') }}
					</p>
				</div>

				<div class="col-sm-6">
					<div class="text-right">
						<p>Realisatie: <a href="https://www.firmaq.nl/nl/" target="_blank">Firmaq media</a></p>
					</div>
				</div>

			</div><!-- .row -->

		</div>

	</div>
</footer>
<!-- END FOOTER -->
