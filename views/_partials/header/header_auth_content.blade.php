<!-- LOGO -->
<div class="inner-header">
	<a class="inner-brand" href="/nl/">
		{!! Theme::img('images/logo@2x.png', 'alt', 'brand-dark', ['width' => '160']) !!}
	</a>
</div>
