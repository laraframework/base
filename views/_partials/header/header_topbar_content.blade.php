<div class="row">

	<div class="col-md-6 hidden-sm hidden-xs">
		<ul class="top-bar-list list-icons">
			<li><i class="icon-paper-plane icons"></i>10 Waterloo Rd, Romford RM1</li>
			<li><i class="icon-envelope-open icons"></i> hello@neomax.com</li>
		</ul>
	</div>

	<div class="col-md-6 col-sm-12 text-right">
		<ul class="top-bar-list list-dividers">
			<li class="top-bar-link"><a href="#">My Account</a></li>
			<li class="top-bar-link"><a href="#">Wishlist</a></li>
			<li class="top-bar-link"><a href="#">Newsletter</a></li>
			<li class="top-bar-link"><a href="#">Signup</a></li>
		</ul>
	</div>

</div>
