<!-- HEADER -->
<header class="header header-fixed header-fixed-transparent header-transparent">
	<div class="container">
		@include('_partials.header.header_content')
	</div>
</header>
<!-- END HEADER -->
