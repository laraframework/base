<div class="top-bar">
	<div class="container">
		@include('_partials.header.header_topbar_content')
	</div>
</div>

<header class="header js-stick">
	<div class="container">
		@include('_partials.header.header_content')
	</div>
</header>

