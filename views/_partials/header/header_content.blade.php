<!-- LOGO -->
<div class="inner-header">
	<a class="inner-brand" href="/nl/">
		{!! Theme::img('images/logo-light.png', 'alt', 'brand-light', ['width' => '160']) !!}
		{!! Theme::img('images/logo-dark.png', 'alt', 'brand-dark', ['width' => '160']) !!}
	</a>
</div>

<!-- MOBILE MENU -->
<div class="main-nav-toggle">
	<div class="nav-icon-toggle" data-toggle="collapse" data-target="#custom-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
	</div>
</div>

<!-- WIDGETS MENU -->
<div class="inner-header pull-right">
	<div class="menu-extras clearfix">

		@if(config('lara.auth.has_front_auth'))
			<hx:include src="/loginwidget/menu"></hx:include>
		@endif

		<!-- SEARCH -->
		<div class="menu-item hidden-xxs">
			<div class="extras-search">
				<a id="modal-search" href="#"><i class="icon-magnifier icons"></i></a>

				{{ html()->form('GET', route('special.search.result'))
					->class('header-search-form')
					->attributes(['accept-charset' => 'UTF-8'])
					->open() }}

				<div class="header-search-form-close">
					<a href="#" class="form-close-btn">✕</a>
				</div>
				<div class="search-form-inner">
					<div class="container">
						<div class="row">
							<div class="col-xs-8 col-xs-offset-1 col-sm-7 col-sm-offset-2 p-0">
								<input type="text" name="keywords" placeholder="zoek op trefwoord">
							</div>
							<div class="col-xs-2 col-sm-1 p-0">
								<button type="submit" class="btn">
									<i class="fa fa-search"></i>
								</button>
							</div>
						</div>
					</div>
				</div>

				{{ html()->form()->close() }}

			</div>
		</div>
		<!-- END SEARCH -->

	</div>
</div>

<!-- MAIN MENU -->
<nav id="custom-collapse" class="main-nav collapse clearfix">
	<ul class="inner-nav pull-right">
		@widget('menuWidget', ['mnu' => 'main', 'showroot' => true, 'grid' => $data->grid])
	</ul>

</nav>

{{ html()->form('POST', route('logout'))
	->id('logout-form')
	->attributes(['accept-charset' => 'UTF-8'])
	->open() }}
<hx:include src="/csrf/input"></hx:include>
{{ html()->hidden('redirect', 'special.home.show') }}
{{ html()->form()->close() }}
