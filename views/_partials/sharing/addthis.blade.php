@if($settngz->addthis_id && strlen($settngz->addthis_id) > 3)
	<section class="module-xs">
		<div class="container">
			<div class="row text-center">
				<div class="m-b-20">Deel deze pagina</div>
				<div class="addthis_inline_share_toolbox"></div>
			</div>
		</div>
	</section>
@endif