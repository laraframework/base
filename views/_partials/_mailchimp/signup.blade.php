@if(isset($settngz->mc_form_action_url))

	<!-- Begin Mailchimp Signup Form -->
	<div id="mc_embed_signup">

		<form action="{{ $settngz->mc_form_action_url }}" method="post" id="mc-embedded-subscribe-form"
		      name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

			<div id="mc_embed_signup_scroll">

				<input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="emailadres"
				       required>
				<div style="position: absolute; left: -5000px;" aria-hidden="true">
					<input type="text" name="b_970069d351fa1c6e122989524_3e08331626" tabindex="-1" value="">
				</div>
				<div class="clear">
					<input type="submit" value="Aanmelden" name="subscribe" id="mc-embedded-subscribe" class="button">
				</div>

			</div>

		</form>

	</div>
	<!--End mc_embed_signup-->

@endif