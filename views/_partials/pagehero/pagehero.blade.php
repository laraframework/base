{{-- PAGE HERO IMAGE --}}

@if(!$data->layout->hero && $data->page && $data->page->hasHero())
	<section class="module-xs page-hero-{{ $data->page->hero->herosize }} bg-black-alfa-40 color-white parallax"
	         data-background="{{ _cimg($data->page->hero->filename, 1920, 960) }}">
		<div class="container">
			<div class="row">
				<div class="col-sm-12 text-center">
					<h3 class="text-xl m-b-0">
						@if($data->page->hero->caption)
							{!! strip_tags($data->page->hero->caption)  !!}
						@else
							&nbsp;
						@endif
					</h3>
				</div>
			</div>
		</div>
	</section>
@endif