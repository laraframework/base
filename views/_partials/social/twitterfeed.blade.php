<h6 class="montserrat text-uppercase bottom-line">Latest Tweets</h6>

<a class="twitter-timeline"
   data-height="500"
   data-dnt="true"
   data-theme="dark"
   data-chrome="noheader nofooter noborders transparent noscrollbar"
   href="https://twitter.com/FirmaqMedia">
	Tweets by FirmaqMedia
</a>
<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>