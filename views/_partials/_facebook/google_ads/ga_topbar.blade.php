@if($entity->getEntityKey() == 'page' || $entity->getMethod() == 'index')
<section id="ga-topbar" class="module-xs ga-topbar" style="display:none;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center">
				<a href="#">
					{!! Theme::img('images/ads/ga-top.jpg', 'Google Ads', 'gad-top-banner', ['width' => '960']) !!}
				</a>
			</div>
		</div>

	</div>
</section>
@endif