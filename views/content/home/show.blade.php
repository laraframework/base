@extends('layout')

@section('content')

	@if(!empty($data->object))
		<!-- ABOUT -->
		<section class="module">
			<div class="container">

				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="module-header text-center">
							<h2 class="montserrat text-uppercase">{{ $data->object->title }}</h2>
							<p class="lead divider-line">{!! $data->object->lead !!}</p>
						</div>
					</div>
				</div><!-- .row -->

				@if($data->object->hasFeatured())
					<div class="row">
						<div class="col-sm-12">
							<figure class="m-b-70">
								@include('_img.lazy', ['lzobj' => $data->object->featured, 'lzw' => 1200, 'lzh' => 400, 'ar' => '3by1'])
							</figure>
						</div>
					</div><!-- .row -->
				@endif

				<div class="row">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="lead text-center">{!! $data->object->body !!}</div>
					</div>
				</div><!-- .row -->

				<div class="row">
					<div class="col-sm-12">
						<div class="text-center m-t-10">
							<a href="{{ route($data->eroutes['page']['about']) }}"
							   class="btn btn-lg btn-brand1">Lees meer</a>
						</div>
					</div>
				</div><!-- .row -->

			</div>
		</section>
		<!-- END ABOUT -->
	@endif

	<!-- PORTFOLIO -->
	<section class="module bg-white-dark p-b-0">
		@widget('entityWidget', ['entity_key' => 'portfolio', 'parent' => 'home', 'term' => null, 'needs_image' =>
		true, 'count' => 8, 'grid' => $data->grid])
	</section>
	<!-- END PORTFOLIO -->

	<!-- SERVICES -->
	<section class="module">
		<div class="container">

			<div class="row multi-columns-row">

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home1'])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home2'])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home3'])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home4'])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home5'])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home6'])
				</div>
				<!-- END ICONBOX -->

			</div><!-- .row -->

		</div>
	</section>
	<!-- END SERVICES -->

	<!-- CALLOUT 1 -->
	@widget('ctaWidget', ['hook' => 'homeparallax1', 'template' => 'parallax', 'grid' => $data->grid])
	<!-- END CALLOUT 1 -->

	<!-- FEATURES -->
	<section class="module">
		<div class="container">

			@include('larawidget', ['hook' => 'home7'])

			<div class="row multi-columns-row">

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home8', 'delay' => 0])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home9', 'delay' => 100])
				</div>

				<div class="col-sm-4 col-md-4 col-lg-4">
					@include('larawidget', ['hook' => 'home10', 'delay' => 200])
				</div>

			</div><!-- .row -->

		</div>
	</section>
	<!-- END FEATURES -->

	<!-- TEAM -->
	<section class="module bg-white-dark">
		<div class="container">
			@widget('entityWidget', ['entity_key' => 'team', 'parent' => 'home', 'term' => null, 'needs_image' => true, 'count' => 4, 'grid' => $data->grid])
		</div>
	</section>
	<!-- END TEAM -->

	<!-- CALLOUT 2 -->
	@widget('ctaWidget', ['hook' => 'homeparallax2', 'template' => 'parallax', 'grid' => $data->grid])
	<!-- END CALLOUT 2 -->

	<!-- BLOGS -->
	<section class="module bg-white-dark">
		<div class="container">
			@widget('entityWidget', ['entity_key' => 'blog', 'parent' => 'home', 'term' => 'home-blog-widget',
			'needs_image' => true, 'count' => 3, 'grid' => $data->grid])
		</div>
	</section>
	<!-- END BLOGS -->

	<!-- CLIENTS -->
	<section class="module-xs">
		<div class="container">
			@widget('entityWidget', ['entity_key' => 'sponsor', 'parent' => 'home', 'term' => null, 'needs_image'
			=> true, 'count' => 8, 'grid' => $data->grid])
		</div>
	</section>
	<!-- END CLIENTS -->

@endsection

