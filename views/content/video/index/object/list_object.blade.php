<article>
	<div class="row m-b-40">

		<div class="col-sm-3">
			<figure>
				<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
					@include('_img.youtube', ['ytcode' => $obj->youtubecode, 'ytsize' => 0, 'ytw' => 480, 'yth' => 360])
				</a>
			</figure>
		</div>

		<div class="col-sm-9">
			<h2>
				<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}" class="brand1">
					{{ $obj->title }}
				</a>
			</h2>
			<p>{!! $obj['lead'] !!}</p>
			<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">Lees meer</a>
		</div>

	</div>
</article>

