<article>

	<figure>
		<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
			@include('_img.youtube', ['ytcode' => $obj->youtubecode, 'ytsize' => 0, 'ytw' => 480, 'yth' => 360])
		</a>
	</figure>

	<div class="grid-object-{{ $entity->getEntityKey() }} grey-light-bg p-t-20 p-b-20 m-b-30 text-center">
		<h3>
			<a href="{{ route($entity->getActiveRoute() . '.show', $obj->routeVars) }}">
				{{ $obj->title }}
			</a>
		</h3>
	</div>

</article>