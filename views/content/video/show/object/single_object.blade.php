<h1 class="lara-object-title">{{ $data->object->title }}</h1>

{{-- YOUTUBE VIDEO --}}
<div class="embed-responsive embed-responsive-16by9 m-t-40 m-b-40">
	<iframe width="560" height="315"
	        src="https://www.youtube.com/embed/{{ $data->object->youtubecode }}?rel=0"
	        frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>

{{-- FEATURED IMAGE --}}
@if($data->object->hasFeatured() && !$data->object->heroIsFeatured())
	<figure class="m-b-40">
		@include('_img.lazy', ['lzobj' => $data->object->featured, 'lzw' => 1280, 'lzh' => 640, 'ar' => '2by1', 'fc' => false])
	</figure>
@endif

{{-- BODY TEXT --}}
{!! $data->object->body !!}

{{-- RELATED --}}
@if($entity->hasRelated())
	@include('content._partials.object_related')
@endif
{{-- FILES --}}
@if($data->object->hasFiles())
	@include('content._partials.object_files')
@endif

{{-- MEDIA GALLERY --}}
@if($data->object->hasGallery())
	@include('content._partials.object_gallery')
@endif