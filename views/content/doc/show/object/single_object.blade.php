<h1 class="lara-object-title">{{ $data->object->title }}</h1>

<table class="table table-striped">
	<tbody>
		<tr>
			<td width="25%">Document:</td>
			<td width="75%">{{ $data->object->title }}</td>
		</tr>

		@if($data->object->lead)
			<tr>
				<td width="25%">Beschrijving:</td>
				<td width="75%">{!! $data->object->lead !!}</td>
			</tr>
		@endif

		<tr>
			<td width="25%">Bestand:</td>
			<td width="75%">
				@foreach($data->object->files as $objfile)
					{{ $objfile->title }}
				@endforeach
			</td>
		</tr>
	</tbody>
</table>

<div class="row m-t-40">
	<div class="col-sm-12 text-center">
		@foreach($data->object->files as $objfile)
			<a href="{{ $entity->getUrlForFiles().'/' . $objfile->filename }}"
			   target="_blank" class="btn btn-lg btn-brand1">
				DOWNLOAD
			</a>
		@endforeach
	</div>
</div>