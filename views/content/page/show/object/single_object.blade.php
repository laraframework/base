<h1 class="lara-object-title">{{ $data->object->title }}</h1>

{{-- FEATURED VIDEO --}}
@if($data->object->hasVideofiles())
	<div class="embed-responsive embed-responsive-16by9 m-t-40 m-b-40">
		<video controls>
			@if($data->object->publish == 1)
				<source src="{{ $entity->getUrlForVideos().'/' . $data->object->videofile->filename }}"
				        type="video/mp4">
			@else
				<source src="{{ $entity->getUrlForVideos().'/_archive/' . $data->object->videofile->filename }}"
				        type="video/mp4">
			@endif
		</video>
	</div>
@elseif($data->object->hasVideos())
	<div class="embed-responsive embed-responsive-16by9 m-t-40 m-b-40">
		<iframe width="560" height="315"
		        src="https://www.youtube.com/embed/{{ $data->object->video->youtubecode }}?rel=0"
		        frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
@endif

{{-- FEATURED IMAGE --}}
@if($data->object->hasFeatured() && !$data->object->heroIsFeatured())
	<figure class="m-b-40">
		@include('_img.lazy', ['lzobj' => $data->object->featured, 'lzw' => 1280, 'lzh' => 640, 'ar' => '2by1', 'fc' => false])
	</figure>
@endif

@include('larawidget', ['hook' => 'bodytext_top'])

{{-- BODY TEXT --}}
{!! $data->object->body !!}

@include('larawidget', ['hook' => 'bodytext_bottom'])

{{-- RELATED --}}
@if($entity->hasRelated())
	@include('content._partials.object_related')
@endif

{{-- FILES --}}
@if($data->object->hasFiles())
	@include('content._partials.object_files')
@endif