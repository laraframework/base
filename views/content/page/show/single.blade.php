@include('larawidget', ['hook' => 'content_top'])

<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">

		<div class="row">

			{{-- Sidebar Left --}}
			@includeWhen($data->grid->hasSidebarLeft, 'content._sidebars.left_default')

			<div class="{{ $data->grid->contentCols }} main-content">

				<div class="row">
					<div class="{{ $data->grid->gridColumns }}">

						@include('content.' . $entity->getEntityKey() . '.show.object.single_object')

					</div>
				</div>

			</div>

			{{-- Sidebar Right --}}
			@includeWhen($data->grid->hasSidebarRight, 'content._sidebars.right_default')

		</div>

	</div>
</section>

{{-- if the layout is 'boxed', you can 'break out' by starting a new section --}}
@if($data->object->hasGallery())
	<section class="{{ $data->grid->module }}">
		<div class="container-fluid full-width">
			@include('content._partials.object_gallery')
		</div>
	</section>
@endif

@include('larawidget', ['hook' => 'content_bottom'])