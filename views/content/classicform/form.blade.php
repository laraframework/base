@extends('layout')

@section('head-after')
	{!! Theme::css('js/vendor/iCheck/lara/blue.css') !!}
	{!! Theme::css('js/vendor/iCheck/lara/aero.css') !!}
	{!! Theme::css('js/vendor/datepicker/bootstrap-datetimepicker.css') !!}
	<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')

	@include('content.'.$entity->getEntityKey().'.show.show')

@endsection


@section('scripts-after')

	<!-- iCheck -->
	{!! Theme::js('js/vendor/iCheck/icheck.min.js') !!}

	<!-- DatePicker -->
	{!! Theme::js('js/vendor/datepicker/moment.js') !!}
	{!! Theme::js('js/vendor/datepicker/bootstrap-datetimepicker.js') !!}

	<script>
		$(document).ready(function () {

			// iCheck
			$('input.icheckjs').iCheck({
				checkboxClass: 'icheckbox_lara-blue',
				radioClass: 'iradio_lara-blue'
			});

			@foreach ($entity->getCustomColumns() as $cvar)
				@if ($cvar->fieldtype == 'date')
					// datetimepicker
					$('#dtp-{{  $cvar->fieldname }}').datetimepicker({
						format: 'YYYY-MM-DD'
					});
				@endif
			@endforeach

		});
	</script>

@endsection