<?php
$reqMessage = _lanq('lara-front::default.form.error_required');
$emailReqMessage = _lanq('lara-front::default.form.error_email_is_invalid');
?>

<div class="text-right p-b-20">
	* = {{ _lanq('lara-front::default.form.required') }}
</div>

@if ($errors->any())
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
@endif

<form id="{{ $entity->getEntityKey() }}-form" method="post" role="form" data-toggle="validator">

	@if(config('lara.httpcache_on_forms'))
		<hx:include src="/csrf/input"></hx:include>
	@else
		@csrf
	@endif

	<x-honeypot/>

	@foreach($entity->getCustomColumns() as $cvar)
		@if($cvar->fieldstate == 'enabled')

			<x-frontformrow>
				<x-slot name="label">
					{{ html()->label(_lanq('lara-front::'.$entity->getEntityKey().'.formfield.' .$cvar->fieldname) .':', $cvar->fieldname) }}
					@if($cvar->required)
						*
					@endif

				</x-slot>

				@if($cvar->fieldtype == 'string')
					{{ html()->text($cvar->fieldname, null)
						->class('form-control')
						->if($cvar->required, function ($el) use ($reqMessage) {
							return $el->data('error', $reqMessage)->required();
						}) }}
				@endif

				@if($cvar->fieldtype == 'email')
					{{ html()->email($cvar->fieldname, null)
						->class('form-control')
						->if($cvar->required, function ($el) use ($emailReqMessage) {
							return $el->data('error', $emailReqMessage)->required();
						}) }}
				@endif

				@if($cvar->fieldtype == 'text')
					{{ html()->textarea($cvar->fieldname, null)
					->class('form-control')
					->rows(4)
					->if($cvar->required, function ($el) use ($reqMessage) {
						return $el->data('error', $reqMessage)->required();
					}) }}
				@endif

				@if($cvar->fieldtype == 'integer')
					{{ html()->input('number', $cvar->fieldname, null)
						->class('form-control')
						->attributes(['step' => '1'])
						->if($cvar->required, function ($el) use ($reqMessage) {
							return $el->data('error', $reqMessage)->required();
						}) }}
				@endif

				@if($cvar->fieldtype == 'boolean')
					{{ html()->hidden($cvar->fieldname, 0) }}
						{{ html()->checkbox($cvar->fieldname, null, 1)
							->class('form-control icheckjs')
							->if($cvar->required, function ($el) use ($reqMessage) {
								return $el->data('error', $reqMessage)->required();
							}) }}
				@endif

			</x-frontformrow>
		@else

				@if($cvar->fieldtype == 'boolean')
				{{ html()->hidden($cvar->fieldname, 0) }}
			@else
				{{ html()->hidden($cvar->fieldname, null) }}
			@endif

		@endif
	@endforeach

	@if(config('app.env') == 'production' && config('lara.google_recaptcha_site_key'))

		<x-frontshowrow>
			<x-slot name="label">
				&nbsp;
			</x-slot>

			<div class="g-recaptcha"
			     data-sitekey="{{ config('lara.google_recaptcha_site_key') }}"
			     data-callback="verifyRecaptchaCallback"
			     data-expired-callback="expiredRecaptchaCallback"></div>

			<!-- extra field for triggering the validator -->
			<input class="form-control hidden" data-recaptcha="true" required=""
			       data-error="U moet nog aanvinken dat u geen robot bent.">

		</x-frontshowrow>

	@endif

	{{ html()->hidden('_ipaddress', Request::ip()) }}

	<button id="{{ $entity->getEntityKey() }}-submit-button" type="submit"
	        class="btn btn-lg btn-info pull-right">{{ _lanq('lara-front::'.$entity->getEntityKey().'.button.submit') }}</button>

</form>

<!-- Ajax response -->
<div id="{{ $entity->getEntityKey() }}-response" class="ajax-response text-center"></div>