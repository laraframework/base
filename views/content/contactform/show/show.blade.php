<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">

		<div class="row">

			{{-- Sidebar Left --}}
			@includeWhen($data->grid->hasSidebarLeft, 'content._sidebars.left_default')

			<div class="{{ $data->grid->contentCols }} main-content">

				<div class="row">
					<div class="{{ $data->grid->gridColumns }}">
						<h1 class="lara-object-title">{{ $data->page->title }}</h1>
						{!! $data->page->body !!}
					</div>
				</div>

				<div class="row">
					<div class="{{ $data->grid->gridColumns }}">

						@include('content.' . $entity->getEntityKey() . '.show.form.form')

					</div>
				</div>

				<div class="row m-t-80 m-b-60">
					<div class="col-sm-12">
						<div class="maps-container no-map-scroll">

							<div class="click-map"></div>
							<div id="map-canvas" style="width:100%; height:500px; border:solid 1px #ccc"></div>
						</div>
					</div>
				</div>

			</div>

			{{-- Sidebar Right --}}
			@includeWhen($data->grid->hasSidebarRight, 'content._sidebars.right_default')

		</div>

	</div>
</section>
