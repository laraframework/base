@extends('layout')

@section('head-after')
	{!! Theme::css('js/vendor/iCheck/lara/blue.css') !!}
	{!! Theme::css('js/vendor/iCheck/lara/aero.css') !!}
	{!! Theme::css('js/vendor/datepicker/bootstrap-datetimepicker.css') !!}
	<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')

	@include('content.'.$entity->getEntityKey().'.show.show')

@endsection


@section('scripts-after')

	<!-- iCheck -->
	{!! Theme::js('js/vendor/iCheck/icheck.min.js') !!}

	<!-- DatePicker -->
	{!! Theme::js('js/vendor/datepicker/moment.js') !!}
	{!! Theme::js('js/vendor/datepicker/bootstrap-datetimepicker.js') !!}


	@if($settngz->company_latitude && $settngz->company_longitude)

	<script>

		function initMap() {

			var myLat = {{ $settngz->company_latitude }};
			var myLong = {{ $settngz->company_longitude }};
			var myZoom = {{ $settngz->google_maps_zoom }};

			var myLocation = {lat: myLat, lng: myLong};
			var map = new google.maps.Map(document.getElementById('map-canvas'), {
				zoom: myZoom,
				center: myLocation
			});

			var myInfo = '<div class="gmapInfoContent">' +
				'<h5>{{ $settngz->company_name }}</h5>' +
				'<p>{{ $settngz->company_street }} {{ $settngz->company_street_nr }}<br>' +
				'{{ $settngz->company_pcode }} {{ $settngz->company_city }}<br>' +
				'<a href="{{ $settngz->company_url }}">{{ $settngz->company_url }}</a></p>' +
				'</div>';



			var infowindow = new google.maps.InfoWindow({
				content: myInfo
			});

			var marker = new google.maps.Marker({
				position: myLocation,
				map: map,
				title: '{{ $settngz->company_name }}'
			});
			marker.addListener('click', function() {
				infowindow.open(map, marker);
			});
		}
	</script>

	<script async defer
	        src="https://maps.googleapis.com/maps/api/js?key={{ config('lara.google_maps_api_key') }}&callback=initMap">
	</script>

	@endif

	<script>
		$(document).ready(function () {

			// use extra field for triggering the validator
			window.verifyRecaptchaCallback = function (response) {
				$('input[data-recaptcha]').val(response).trigger('change');
			}
			window.expiredRecaptchaCallback = function () {
				$('input[data-recaptcha]').val("").trigger('change');
			}

			// iCheck
			$('input.icheckjs').iCheck({
				checkboxClass: 'icheckbox_lara-blue',
				radioClass: 'iradio_lara-blue'
			});

			@foreach ($entity->getCustomColumns() as $cvar)
				@if ($cvar->fieldtype == 'date')
					// datetimepicker
					$('#dtp-{{  $cvar->fieldname }}').datetimepicker({
						format: 'YYYY-MM-DD'
					});
				@endif
			@endforeach

			var formID = '#{{ $entity->getEntityKey() }}-form'
			var form = $(formID);

			var submitID = '#{{ $entity->getEntityKey() }}-submit-button';
			var submit = $(submitID);
			var submittext = submit.html();

			var ajaxResponseID = '#{{ $entity->getEntityKey() }}-response';
			var ajaxResponse = $(ajaxResponseID);


			form.validator().on('submit', function (e) {
				if (e.isDefaultPrevented()) {

					// handle the invalid form...

				} else {

					e.preventDefault();

					var formdata = form.serializeArray();
					@if(config('app.env') == 'production' && config('lara.google_recaptcha_site_key'))
						formdata.push({name: 'rcresponse', value: grecaptcha.getResponse()});
					@endif

					$.ajaxSetup({
						headers: {
							'X-CSRF-TOKEN': $("#{{ $entity->getEntityKey() }}-form input[name=_token]").val()
						}
					});

					$.ajax({
						type: 'POST',
						url: "{{ route('ajax.'.$entity->getEntityKey().'.process') }}",
						dataType: 'json',
						data: formdata,
						cache: false,
						beforeSend: function (result) {
							submit.empty();
							submit.append('<i class="fa fa-circle-o-notch fa-spin"></i>');
						},
						error: function (result) {
							alert(JSON.stringify(result));
						},
						success: function (result) {

							if (result.sendstatus == 1) {
								ajaxResponse.css('padding-top', '25px');
								ajaxResponse.css('margin-bottom', '300px');
								ajaxResponse.css('color', '#C8004B');
								ajaxResponse.css('background-color', '#f0f5fa');
								ajaxResponse.css('min-height', '80px');
								ajaxResponse.html(result.message);
								form.fadeOut(500);
								$('html,body').animate({
									scrollTop: ajaxResponse.offset().top - 200
								});
							} else {
								ajaxResponse.css('color', '#C8004B');
								ajaxResponse.html(result.message);
								submit.empty();
								submit.append(submittext);
							}
						}
					});
				}
			});

		});
	</script>

@endsection