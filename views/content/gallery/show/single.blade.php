<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">

		<div class="row">

			{{-- Sidebar Left --}}
			@includeWhen($data->grid->hasSidebarLeft, 'content._sidebars.left_default')

			<div class="{{ $data->grid->contentCols }} main-content">

				<div class="row">
					<div class="{{ $data->grid->gridColumns }}">
						<a href="{{ $data->entityListUrl }}"
						   class="btn btn-base btn-outline pull-right">{{ _lanq('lara-front::default.button.go_back') }}</a>
					</div>
				</div>
				<div class="row">
					<div class="{{ $data->grid->gridColumns }}">
						<h1 class="lara-object-title">{{ $data->object->title }}</h1>

						{{-- MEDIA GALLERY --}}
						@if($data->object->hasGallery())
							@include('content._partials.object_gallery')
						@endif

					</div>
				</div>

			</div>

			{{-- Sidebar Right --}}
			@includeWhen($data->grid->hasSidebarRight, 'content._sidebars.right_default')

		</div>

	</div>
</section>
