<div class="qlightbox block-grid-xs-2 block-grid-sm-2 block-grid-md-4 m-t-40">
	@foreach($data->object->gallery as $img)
		<div class="gallery-item">
			<a href="{{ _cimg($img->filename, 1920, 1280) }}" title="">
				@include('_img.lazy', ['lzobj' => $img, 'lzw' => 640, 'lzh' => 480, 'ar' => '4by3'])
			</a>
		</div>
	@endforeach
</div>