<div class="row m-t-25">
	<div class="col-sm-6">
		@if($data->prev)
			<a href="{{ route($entity->getActiveRoute(), $data->prev->slug) }}" class="btn btn-primary">
				<i class="icon-arrow-left icons font-12"></i> {{ _lanq('lara-front::default.button.page_prev') }}
			</a>
		@endif
	</div>
	<div class="col-sm-6 text-right">
		@if($data->next)
			<a href="{{ route($entity->getActiveRoute(), $data->next->slug) }}" class="btn btn-primary">
				{{ _lanq('lara-front::default.button.page_next') }} <i class="icon-arrow-right icons font-12"></i>
			</a>
		@endif
	</div>
</div>