<h1 class="lara-object-title">{{ $data->object->title }}</h1>

{{-- FEATURED VIDEO --}}
@if($data->object->hasVideofiles())
	<div class="embed-responsive embed-responsive-16by9 m-t-40 m-b-40">
		<video controls>
			@if($data->object->publish == 1)
				<source src="{{ $entity->getUrlForVideos().'/' . $data->object->videofile->filename }}"
				        type="video/mp4">
			@else
				<source src="{{ $entity->getUrlForVideos().'/_archive/' . $data->object->videofile->filename }}"
				        type="video/mp4">
			@endif
		</video>
	</div>
@elseif($data->object->hasVideos())
	<div class="embed-responsive embed-responsive-16by9 m-t-40 m-b-40">
		<iframe width="560" height="315"
		        src="https://www.youtube.com/embed/{{ $data->object->video->youtubecode }}?rel=0"
		        frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
@endif

{{-- FEATURED IMAGE --}}
@if($data->object->hasFeatured() && !$data->object->heroIsFeatured())
	<figure class="m-b-40">
		@include('_img.lazy', ['lzobj' => $data->object->featured, 'lzw' => 1280, 'lzh' => 640, 'ar' => '2by1', 'fc' => false])
	</figure>
@endif

<table class="table">
	<tr>
		<th style="width:20%">&nbsp;</th>
		<th style="width:20%">&nbsp;</th>
		<th style="width:60%">&nbsp;</th>
	</tr>
	<tr>
		<td class="event-table-grey">
			<div class="event-date-container">
				<div class="event-date-month">
					{{ Carbon\Carbon::parse( $data->object->startdate)->format('M') }}
				</div>
				<div class="event-date-day">
					{{ Carbon\Carbon::parse( $data->object->startdate)->format('d') }}
				</div>
			</div>
		</td>
		<td colspan="2" class="event-table-grey p-t-25">
			{!! $data->object->title !!}
		</td>
	</tr>

	@if($data->object->enddate == $data->object->startdate)

		<tr>
			<td class="event-table-white">&nbsp;</td>
			<td class="event-table-white">Datum</td>
			<td class="event-table-white">
				{{ Date::parse($data->object->startdate)->format('j F Y') }}
			</td>
		</tr>
		@if($data->object->starttime != '00:00:00')
			<tr>
				<td class="event-table-white">&nbsp;</td>
				<td class="event-table-white">Tijd</td>
				<td class="event-table-white">

					{{ Carbon\Carbon::parse( $data->object->starttime)->format('H:i') }}
					@if($data->object->endtime && $data->object->endtime != $data->object->starttime)
						&nbsp;-&nbsp;{{ Carbon\Carbon::parse( $data->object->endtime)->format('H:i') }}
					@endif
					&nbsp;uur

				</td>
			</tr>
		@endif
	@else

		<tr>
			<td class="event-table-white">&nbsp;</td>
			<td class="event-table-white">Startdatum</td>
			<td class="event-table-white">
				{{ Date::parse($data->object->startdate)->format('j F Y') }}
				@if($data->object->starttime != '00:00:00')
					&nbsp;-&nbsp;
					{{ Carbon\Carbon::parse( $data->object->starttime)->format('H:i') }}
					&nbsp;uur
				@endif
			</td>
		</tr>
		<tr>
			<td class="event-table-white">&nbsp;</td>
			<td class="event-table-white">Enddatum</td>
			<td class="event-table-white">
				{{ Date::parse($data->object->enddate)->format('j F Y') }}
				@if($data->object->endtime != '00:00:00')
					&nbsp;-&nbsp;
					{{ Carbon\Carbon::parse( $data->object->endtime)->format('H:i') }}
					&nbsp;uur
				@endif
			</td>
		</tr>

	@endif

	@if($data->object->body)
		<tr>
			<td class="event-table-white">&nbsp;</td>
			<td class="event-table-white">Bijzonderheden</td>
			<td class="event-table-white">
				{!! $data->object->body !!}
			</td>
		</tr>
	@endif

	<tr>
		<td colspan="3" class="event-table-grey">&nbsp;</td>
	</tr>

</table>

{{-- RELATED --}}
@if($entity->hasRelated())
	@include('content._partials.object_related')
@endif
{{-- FILES --}}
@if($data->object->hasFiles())
	@include('content._partials.object_files')
@endif

{{-- MEDIA GALLERY --}}
@if($data->object->hasGallery())
	@include('content._partials.object_gallery')
@endif