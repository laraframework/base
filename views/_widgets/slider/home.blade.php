@if($widgetsliders->isNotEmpty())

	<!-- HERO -->
	<div id="home" class="flexslider {{ $sliderclass }}">
		<ul class="slides">

			@foreach($widgetsliders as $widgetslider)

				@if($widgetslider->type == 'payoff')

					@if($widgetslider->textposition == 'left' || $widgetslider->textposition == 'right')

						@if($widgetslider->overlaysize == 'block')

							{{-- BLOCK LEFT/RIGHT --}}
							<li style="background-image:url({{ _cimg($widgetslider->featured->filename, 1920, 960) }})">

								<div class="hero-container hero-textcolor-{{ $widgetslider->overlaycolor }}">

									<div class="hero-container-inner-{{ $widgetslider->textposition }}">

										<div class="hero-caption">
											<div class="hero-text">

												<div class="row m-0">
													<div class="col-sm-10 @if($widgetslider->textposition == 'left') col-sm-offset-2 @endif text-center">

														<div class="hero-text-bg bg-{{ $widgetslider->overlaycolor }}-alfa-{{ $widgetslider->overlaytransp }}">
															<div class="hero-text-inner">

																<h1 class="montserrat text-xxl m-b-70">{{ $widgetslider->title }}</h1>

																{!! $widgetslider->payoff  !!}

																@if(!empty($widgetslider->url))
																	<div class="btn-list m-t-45">
																		<a href="{{ $widgetslider->url }}"
																		   class="btn btn-circle btn-outline btn-lg @if($widgetslider->overlaycolor == 'white') btn-dark @else btn-white @endif "
																		   title="{{ $widgetslider->urltitle }}">
																			{{ $widgetslider->urltext }}
																		</a>
																	</div>
																@endif
															</div>

														</div>

													</div>
												</div>

											</div>
										</div>

									</div>

								</div>

							</li>

						@else

							{{-- FULL LEFT/RIGHT --}}
							<li style="background-image:url({{ _cimg($widgetslider->featured->filename, 1920, 960) }})">

								<div class="hero-container hero-textcolor-{{ $widgetslider->overlaycolor }}">

									<div class="hero-container-inner-{{ $widgetslider->textposition }} bg-{{ $widgetslider->overlaycolor }}-alfa-{{ $widgetslider->overlaytransp }}">

										<div class="hero-caption">
											<div class="hero-text">

												<div class="row m-0">
													<div class="col-sm-12 text-center">

														<h1 class="montserrat text-xxl m-b-70">{{ $widgetslider->title }}</h1>

														{!! $widgetslider->payoff  !!}

														@if(!empty($widgetslider->url))
															<div class="btn-list m-t-45">
																<a href="{{ $widgetslider->url }}"
																   class="btn btn-circle btn-outline btn-lg @if($widgetslider->overlaycolor == 'white') btn-dark @else btn-white @endif "
																   title="{{ $widgetslider->urltitle }}">
																	{{ $widgetslider->urltext }}
																</a>
															</div>
														@endif
													</div>

												</div>

											</div>
										</div>

									</div>

								</div>

							</li>

						@endif

					@else

						@if($widgetslider->overlaysize == 'block')

							{{-- BLOCK CENTER --}}
							<li style="background-image:url({{ _cimg($widgetslider->featured->filename, 1920, 960) }})">

								<div class="hero-caption hero-textcolor-{{ $widgetslider->overlaycolor }}">
									<div class="hero-text">

										<div class="container">
											<div class="row">
												<div class="col-sm-12 text-center">

													<div class="hero-text-bg bg-{{ $widgetslider->overlaycolor }}-alfa-{{ $widgetslider->overlaytransp }}">
														<div class="hero-text-inner">

															<h1 class="montserrat text-xxxxxl m-b-70">{{ $widgetslider->title }}</h1>

															{!! $widgetslider->payoff  !!}

															@if(!empty($widgetslider->url))
																<div class="btn-list m-t-45">
																	<a href="{{ $widgetslider->url }}"
																	   class="btn btn-circle btn-outline btn-lg @if($widgetslider->overlaycolor == 'white') btn-dark @else btn-white @endif "
																	   title="{{ $widgetslider->urltitle }}">
																		{{ $widgetslider->urltext }}
																	</a>
																</div>
															@endif

														</div>
													</div>
												</div>

											</div>

										</div>
									</div>
								</div>

							</li>


						@else

							{{-- FULL CENTER --}}
							<li class="bg-{{ $widgetslider->overlaycolor }}-alfa-{{ $widgetslider->overlaytransp }}"
							    style="background-image:url({{ _cimg($widgetslider->featured->filename, 1920, 960) }})">

								<div class="hero-caption hero-textcolor-{{ $widgetslider->overlaycolor }}">
									<div class="hero-text">

										<div class="container">
											<div class="row">
												<div class="col-sm-12 text-center">

													<h1 class="montserrat text-super-xl m-b-70">{{ $widgetslider->title }}</h1>

													{!! $widgetslider->payoff  !!}

													@if(!empty($widgetslider->url))
														<div class="btn-list m-t-45">
															<a href="{{ $widgetslider->url }}"
															   class="btn btn-circle btn-outline btn-lg @if($widgetslider->overlaycolor == 'white') btn-dark @else btn-white @endif "
															   title="{{ $widgetslider->urltitle }}">
																{{ $widgetslider->urltext }}
															</a>
														</div>
													@endif

												</div>
											</div>

										</div>

									</div>
								</div>

							</li>

						@endif

					@endif

				@elseif($widgetslider->type == 'caption')

					<li style="background-image:url({{ _cimg($widgetslider->featured->filename, 1920, 960) }})">

						<div class="hero-caption-bg hero-caption-bg-{{ $widgetslider->captiontype }}">
							<div class="hero-caption-bottom">
								<div class="container">
									<div class="row">
										<div class="col-sm-12 text-center">
											{{ $widgetslider->caption }}
										</div>
									</div>
								</div>
							</div>
						</div>

					</li>

				@else

					<li style="background-image:url({{ _cimg($widgetslider->featured->filename, 1920, 960) }})">

					</li>

				@endif

			@endforeach

		</ul>
	</div>
	<!-- END HERO -->

@endif