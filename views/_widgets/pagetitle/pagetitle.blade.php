@if($widgetpagetitle)
	<section class="module-xs bg-black-alfa-40 color-white parallax"
	         data-background="{{ _cimg($widgetpagetitle->featured->filename, 1920, 960) }}">

		<div class="container">

			<div class="row">
				<div class="col-sm-12 text-center">

					<h3 class="text-xxl m-b-10" style="opacity: 0.8">
						@if($menulevelone)
							{{ $menulevelone->title }}
						@else
							&nbsp;
						@endif
					</h3>

				</div>
			</div>

		</div>
	</section>
@endif