<?php
$lang = LaravelLocalization::getCurrentLocale();
?>

@if($node->depth > 0)
	@if($node->publish == 1)
	<option value="{{ url($lang . '/' . $node->route) }}"
	        @if(in_array($node->id, $activemenu)) selected @endif >
		{{ $node->title }}
	</option>
	@endif
@endif

@if(!$node->isLeaf())
	@foreach($node->children as $node)
		@include('_widgets.menu_mobile_sidebar.menu_render', $node)
	@endforeach
@endif





