@if($tree)

	Snel naar:
	<div class="m-b-25">
		<select class="form-control select2" style="width: 100%;" name="tag_id"
		        onchange="if (this.value) window.location.href=this.value">

			@foreach($tree as $node)
				@include('_widgets.menu_mobile_sidebar.menu_render', $node)
			@endforeach

		</select>
	</div>

@endif
