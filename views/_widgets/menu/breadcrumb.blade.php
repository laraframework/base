
<ul class="breadcrumb">
	@foreach($breadcrumb as $crumb)
		<li>
			<a href="{{ $crumb['route'] }}">
				{{ $crumb['title'] }}
			</a>
		</li>
	@endforeach
</ul>
