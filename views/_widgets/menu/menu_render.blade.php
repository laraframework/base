<?php
$lang = LaravelLocalization::getCurrentLocale();
$nodeclass = ($node->isLeaf() || sizeof($node->children) == 0) ? 'isLeaf' : 'has-submenu';
if ($node->isRoot()) {
	$nodeclass = 'isLeaf';
}
?>

@if($node->isRoot())

	{{-- home --}}
	<li class="isLeaf" id="menu-item-{{ $node->id }}">
		<a href="{{ url($lang . '/') }}" @if(sizeof($activemenu) == 1) class="active" @endif >
			{{ $node->title }}
		</a>
	</li>

	{{-- get level 2 and move it one level up, add it to home --}}
	@if (!$node->isLeaf())
		@if(sizeof($node->children) > 0)
			@foreach ($node->children as $node)
				@include('_widgets.menu.menu_render', $node)
			@endforeach
		@endif
	@endif

@else

	<li class="{{ $nodeclass }}" id="menu-item-{{ $node->id }}">
		<a href="{{ url($lang . '/' . $node->route) }}"
		   @if(in_array($node->id, $activemenu)) class="active" @endif >
			{{ $node->title }}
		</a>

		{{-- get next levels --}}
		@if (!$node->isLeaf())
			@if(sizeof($node->children) > 0)
				<ul class="submenu" id="menu-item-{{ $node->id }}-submenu">
					@foreach ($node->children as $node)
						@include('_widgets.menu.menu_render', $node)
					@endforeach
				</ul>
			@endif
		@endif

	</li>

@endif
