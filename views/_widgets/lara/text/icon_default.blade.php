@if($larawidget)
	<div class="box-icon-{{ $larawidget->iconalign }}"
	     @if($animation->status)
	     data-aos="{{ $animation->class }}"
	     data-aos-offset="{{ $animation->offset }}"
	     data-aos-delay="{{ $animation->delay }}"
	     data-aos-duration="{{ $animation->duration }}"
	     data-aos-easing="{{ $animation->easing }}"
	     @endif
	     style="background-color: #f0f5fa;">
		@if(!empty($larawidget->iconclass))
			<div class="icon-box-icon">
				<i class="{{ $larawidget->iconclass }} icons"></i>
			</div>
		@endif
		<h4>{{ $larawidget->title }}</h4>
		{!! $larawidget->body !!}
	</div>
@endif
