@if($larawidget)
	<h6 class="montserrat text-uppercase bottom-line">{{ $larawidget->title }}</h6>
	{!! $larawidget->body !!}
@endif