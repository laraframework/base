@if($larawidget)
	<div class="box-icon-{{ $larawidget->iconalign }} @if($animation->status) wow {{ $animation->class }} @endif"
	     @if($animation->status)
	     data-wow-offset="{{ $animation->offset }}"
	     data-wow-duration="{{ $animation->duration }}"
	     data-wow-delay="{{ $animation->delay }}"
	     @endif
	     style="padding: 25px; background-color: #f0f5fa;">
		@if(!empty($larawidget->iconclass))
			<div class="icon-box-icon">
				<i class="{{ $larawidget->iconclass }} icons"></i>
			</div>
		@endif
		<h4>{{ $larawidget->title }}</h4>
		{!! $larawidget->body !!}
	</div>
@endif
