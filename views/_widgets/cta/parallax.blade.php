@if($widgetcta && $widgetcta->featured)
	<section
			class="module bg-{{ $widgetcta->overlaycolor }}-alfa-{{ $widgetcta->overlaytransp }} parallax color-white"
			data-background="{{ _cimg($widgetcta->featured->filename, 1920, 1280) }}">
		<div class="container">

			<div class="row">
				<div class="col-sm-12">
					<div class="text-center">
						<h2 class="montserrat text-uppercase m-b-30">{{ $widgetcta->title }}</h2>
						<a href="{{ $widgetcta->linkurl }}"
						   class="btn btn-circle btn-lg btn-white">{{ $widgetcta->linktext }}</a>
					</div>
				</div>
			</div><!-- .row -->

		</div>
	</section>
@endif
