@if($widgetcta)
	<section class="module-xs">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="text-center">
						<h2 class="hfont text-500 letter-spacing-2 m-b-30">{{ $widgetcta->title }}</h2>
						<p>{{ $widgetcta->body }}</p>
						<a href="{{ $widgetcta->linkurl }}"
						   class="btn btn-lg btn-info">
							{{ $widgetcta->linktext }}
						</a>
					</div>
				</div>
			</div><!-- .row -->
		</div>
	</section>
@endif
