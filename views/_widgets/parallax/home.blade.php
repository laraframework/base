@if(!empty($widgetparallax))

	<section id="hero" class="module-hero bg-black-alfa-30 color-white fullheight parallax"
	         data-background="{{ _cimg($widgetparallax->featured->filename, 1920, 1280) }}">

		@if($widgetparallax->type == 'payoff')

			<div class="hero-caption">
				<div class="hero-text">

					<div class="container">

						<div class="row">
							<div class="col-sm-12 text-center">

								<h1 class="montserrat text-xxl m-b-70">{{ $widgetparallax->title }}</h1>

								{!! $widgetparallax->payoff  !!}

								@if(!empty($widgetparallax->url))
									<div class="btn-list m-t-45">
										<a href="{{ $widgetparallax->url }}"
										   class="btn btn-circle btn-outline btn-lg @if($widgetparallax->overlaycolor == 'white') btn-dark @else btn-white @endif "
										   title="{{ $widgetparallax->urltitle }}">
											{{ $widgetparallax->urltext }}
										</a>
									</div>
								@endif

							</div>
						</div>

					</div>

				</div>
			</div>

		@endif

	</section>

@endif
