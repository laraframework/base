@if($widgetObjects->isNotEmpty())
	<div class="row">
		<div class="col-sm-12">

			<div class="clients-carousel">

				@foreach($widgetObjects as $widgetObject)

					@if($widgetObject->hasFeatured())
						<a href="#">
							@include('_img.lazy', ['lzobj' => $widgetObject->featured, 'lzw' => 120, 'lzh' => 90, 'ar' => '4by3'])
						</a>
					@endif

				@endforeach

			</div>
			<!-- END CLIENTS CAROUSEL -->

		</div>
	</div><!-- .row -->
@endif