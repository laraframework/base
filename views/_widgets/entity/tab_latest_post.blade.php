@if($widgetObjects->isNotEmpty())

	<div class="single-post-tab-list">
		<div class="row multi-columns-row single-post-tab-list">
			@foreach($widgetObjects as $widgetObject)
				<div class="col-sm-6 col-md-6 col-lg-6">
					<a href="{{ route($widgetEntityRoute . '.show', $widgetObject->slug) }}"
					   class="single-post-tab-item">
						<div class="row m-b-30">
							<div class="col-xs-4" style="padding-right:6px;">
								@include('content._partials.thumbnail', ['thumb' => $widgetObject, 'twidth' => 640, 'theight' => 360])
							</div>
							<div class="col-xs-8" style="padding-left:6px;">
								<div class="tab-item-title">
									{{ $widgetObject->title }}
								</div>
								<div class="tab-item-lead">
									{!! str_limit(strip_tags($widgetObject->lead), 50, ' ...') !!}
								</div>
							</div>
						</div>
					</a>
				</div>
			@endforeach
		</div>
	</div>

@endif
