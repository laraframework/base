@if($widgetObjects->isNotEmpty())
	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="module-header text-center">
				<h2 class="montserrat text-uppercase">{{ _lanq('lara-front::home.blogwidget.title') }}</h2>
				<p class="lead divider-line">{{ _lanq('lara-front::home.blogwidget.lead') }}</p>
			</div>
		</div>
	</div>

	<div class="row multi-columns-row post-columns">

		@foreach($widgetObjects as $widgetObject)

			<div class="col-sm-4 col-md-4 col-lg-4">
				<article class="post format-image bg-white">

					@if($widgetObject->hasFeatured())
						<div class="post-preview">
							<a href="{{ route($widgetEntityRoute . '.show', $widgetObject->slug) }}">
								@include('_img.lazy', ['lzobj' => $widgetObject->featured, 'lzw' => 960, 'lzh' => 480, 'ar' => '2by1'])
							</a>
						</div>
					@endif

					<div class="post-content">
						<h2 class="post-title">
							<a href="{{ route($widgetEntityRoute . '.show', $widgetObject->slug) }}">
								{{ $widgetObject->title }}
							</a>
						</h2>
						<ul class="post-meta">
							<li>{{ Carbon\Carbon::parse($widgetObject->publish_from)->format('d F Y') }}</li>
							<li>By <a href="#">{{ $widgetObject->user->name }}</a></li>
						</ul>
						<p>{!! $widgetObject->lead  !!}</p>
						<a href="{{ route($widgetEntityRoute . '.show', $widgetObject->slug) }}"
						   class="btn btn-lg btn-link btn-base">
							{{ _lanq('lara-front::home.blogwidget.readmore') }}
						</a>
					</div>

				</article>
			</div>
			<!-- END POST IMAGE -->

		@endforeach

	</div><!-- .row -->

	@if($widgetEntityRoute)
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center m-t-35">
					<a href="{{ route($widgetEntityRoute) }}"
					   class="btn btn-circle btn-lg btn-fade btn-base">{{ _lanq('lara-front::home.blogwidget.morenewsurl') }}</a>
				</div>
			</div>
		</div><!-- .row -->
	@endif

@endif
