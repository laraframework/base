@if($widgetObjects->isNotEmpty())

	<div class="row">
		<div class="col-sm-6 col-sm-offset-3">
			<div class="module-header text-center">
				<h2 class="montserrat text-uppercase">Team</h2>
				<p class="lead divider-line">Creative people with the amazing skills.</p>
			</div>
		</div>
	</div>

	<div class="row">

		@foreach($widgetObjects as $widgetObject)

			<div class="col-sm-6 col-md-3">
				<div class="team-item m-b-sm-35">
					<div class="team-photo">

						@if($widgetObject->hasFeatured())
							@include('_img.lazy', ['lzobj' => $widgetObject->featured, 'lzw' => 600, 'lzh' => 800, 'ar' => '3by4'])
						@endif

						<div class="team-social">
							<div>
								<div>
									<a href="#"><i class="fa fa-facebook"></i></a>
									<a href="#"><i class="fa fa-twitter"></i></a>
									<a href="#"><i class="fa fa-dribbble"></i></a>
								</div>
							</div>
						</div>
					</div>
					<div class="team-inner">
						<h6 class="team-name">{{ $widgetObject->firstname }} {{ $widgetObject->middlename }} {{ $widgetObject->title }}</h6>
						<span class="team-role">{{ $widgetObject->role }}</span>
					</div>
				</div>
			</div>
			<!-- END TEAM MEMBER -->

		@endforeach
	</div><!-- .row -->

@endif
