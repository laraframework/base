@if($widgetObjects->isNotEmpty())

	<div class="container">

		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="module-header text-center">
					<h2 class="montserrat text-uppercase">Portfolio</h2>
					<p class="lead divider-line">An eye for detail makes our works beautiful.</p>
				</div>
			</div>
		</div>

		<!-- PORTFOLIO FILTERS -->
		<div class="row">
			<div class="col-sm-12">
				<ul id="filters" class="filters">
					<li><a href="#" class="current" data-filter="*">All</a></li>
					@if($widgetTaxonomies)
						@foreach($widgetTaxonomies as $widgetTaxonomy)
							<li><a href="#" data-filter=".{{ $widgetTaxonomy->slug }}">{{ $widgetTaxonomy->title }}</a>
							</li>
						@endforeach
					@endif
				</ul>
			</div>
		</div>
		<!-- END PORTFOLIO FILTERS -->

	</div>

	<!-- WORKS WRAPPER -->
	<div class="works-grid-wrapper">

		<!-- WORKS GRID -->
		<div id="works-grid" class="works-grid works-grid-4">

			@foreach($widgetObjects as $widgetObject)

				<article class="work-item {{ $widgetObject->tags()->first()->slug }}">
					<div class="work-wrapper">
						@if($widgetObject->hasFeatured())
							@include('_img.lazy', ['lzobj' => $widgetObject->featured, 'lzw' => 720, 'lzh' => 540, 'ar' => '4by3'])
						@endif
						<div class="work-overlay">
							<div class="work-caption">
								<h6 class="work-title">{{ $widgetObject->title }}</h6>
								<span class="work-category">
									{{ $widgetObject->tags()->first()->title }}
								</span>
							</div>
						</div>
						<a href="portfolio-single-1.html" class="work-link"></a>
					</div>
				</article>
				<!-- END PORTFOLIO ITEM -->

			@endforeach

		</div>
		<!-- END WORKS GRID -->

	</div>
	<!-- END WORKS WRAPPER -->

@endif
