@if($widgetObjects->isNotEmpty())

	<ul class="widget-list">
		@foreach($widgetObjects as $widgetObject)
			<li class="widget-item">
				<a href="{{ route('entitytag.post.index.show', $widgetObject->slug) }}">
					<div class="row m-b-10">
						<div class="col-xs-4" style="padding-right:6px;">
							@include('content._partials.thumbnail', ['thumb' => $widgetObject, 'twidth' => 640, 'theight' => 360])
						</div>
						<div class="col-xs-8" style="padding-left:6px;">
							<div class="widget-item-title">
								{{ $widgetObject->title }}
							</div>
							<div class="widget-item-lead">
								{!! str_limit(strip_tags($widgetObject->lead), 50, ' ...') !!}
							</div>
						</div>
					</div>
				</a>
			</li>
		@endforeach
	</ul>

@endif
