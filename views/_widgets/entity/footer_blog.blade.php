@if($widgetObjects->isNotEmpty())

	<h6 class="montserrat text-uppercase bottom-line">Recent Posts</h6>

	<ul class="recent-posts">

		@foreach($widgetObjects as $widgetObject)

			<li>
				@if($widgetObject->hasFeatured())
					<div class="widget-posts-image">
						<a href="{{ route($widgetEntityRoute . '.show', $widgetObject->slug) }}">
							@include('_img.lazy', ['lzobj' => $widgetObject->featured, 'lzw' => 960, 'lzh' => 480, 'ar' => '2by1'])
						</a>
					</div>
				@endif

				<div class="widget-posts-body">
					<h6 class="widget-posts-title">
						<a href="{{ route($widgetEntityRoute . '.show', $widgetObject->slug) }}">{{ $widgetObject->title }}</a>
					</h6>
					<div class="widget-posts-meta">{{ Date::parse($widgetObject->publish_from)->format('j F Y') }}</div>
				</div>
			</li>

		@endforeach

	</ul>

@endif

