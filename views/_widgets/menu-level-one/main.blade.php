@php
	$lang = LaravelLocalization::getCurrentLocale();
@endphp
<ul>
	@foreach($menulevelone as $item)
		@if($item->publish == 1)
		<li>
			<a href="{{ url($lang . '/' . $item->route) }}">
				{{ $item->title }}
			</a>
		</li>
		@endif
	@endforeach
</ul>
