<?php
$lang = LaravelLocalization::getCurrentLocale();
$nodeclass = $node->isLeaf() ? 'isLeaf' : 'has-submenu';
?>

<li class="{{ $nodeclass }} @if(in_array($node->id, $activemenu)) submenu-open @endif" id="menu-item-{{ $node->id }}">

	@if($node->isRoot())

		{{-- home --}}
		<a href="{{ url($lang . '/') }}" @if(sizeof($activemenu) == 1) class="active" @endif >
			{{ $node->title }}
		</a>

		{{-- get level 2 and add it to home (without ul) --}}
		@if (!$node->isLeaf())
			@if(!empty($node->children))
				@foreach ($node->children as $node)
					@include('_widgets.menu_sidebar.menu_render', $node)
				@endforeach
			@endif
		@endif

	@else
		@if($node->publish == 1)
			<a href="{{ url($lang . '/' . $node->route) }}" id="test"
			   @if(in_array($node->id, $activemenu)) class="active" @endif >
				{{ $node->title }}
			</a>
		@endif
	@endif

	{{-- get level 3, 4, etc --}}
	@if (!$node->isLeaf())
		<ul class="submenu" id="menu-item-{{ $node->id }}-submenu">
			@if(!empty($node->children))
				@foreach ($node->children as $node)
					@include('_widgets.menu_sidebar.menu_render', $node)
				@endforeach
			@endif
		</ul>
	@endif

</li>


