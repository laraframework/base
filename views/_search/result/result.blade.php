<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">

		<div class="row">

			{{-- Sidebar Left --}}
			@includeWhen($data->grid->hasSidebarLeft, 'content._sidebars.left_default')

			<div class="{{ $data->grid->contentCols }} main-content">

				@include('larawidget', ['hook' => 'content_top'])

				<div class="row">
					<div class="{{ $data->grid->gridColumns }}">

						<h1>Zoekresultaten</h1>

						@if($data->singleEntity)

							{{ html()->form('GET', route('special.search.modresult', ['module' => $data->singleEntity->getEntityKey()]))
								->attributes(['accept-charset' => 'UTF-8'])
								->open() }}

							<div class="row form-group">
								<div class="col-sm-10">
									{{ html()->text('keywords', $data->keywords)->class('form-control') }}
								</div>
								<div class="col-sm-2">
									{{ html()->button('Go', 'submit')->class('btn btn-brand1 btn-flat save-button') }}
								</div>
							</div>

							{{ html()->form()->close() }}

						@else

							{{ html()->form('GET', route('special.search.result'))
								->attributes(['accept-charset' => 'UTF-8'])
								->open() }}

							<div class="row form-group">
								<div class="col-sm-10">
									{{ html()->text('keywords', $data->keywords)->class('form-control') }}
								</div>
								<div class="col-sm-2">
									{{ html()->button('Go', 'submit')->class('btn btn-brand1 btn-flat save-button') }}
								</div>
							</div>

							{{ html()->form()->close() }}

						@endif

						@foreach($data->results as $entity_key => $ent)

							@if($ent->objects->count() > 0)

								<div class="row">
									<div class="col-sm-9 col-sm-offset-3">
										<h2 class="m-b-25 m-t-40">
											{{ ucfirst(_lanq('lara-' . $ent->entity->getModule() . '::' . $entity_key . '.entity.entity_title')) }}
										</h2>
									</div>
								</div>

								@foreach($ent->objects as $result)

									<div class="row p-t-20 p-b-20" style="border-bottom:solid 1px #eee;">
										<div class="col-sm-3">
											@if($result->hasFeatured())
												<img data-src="{{ _cimg($result->featured->filename, 180, 180) }}"
												     alt="{{ $result->featured->image_alt }}"
												     title="{{ $result->featured->image_title }}"
												     width="180" height="180" class="lazyload" />
											@else
												{!! Theme::img('images/logo@2x.png', 'alt', 'brand', ['width' => '480']) !!}
											@endif
										</div>
										<div class="col-sm-9">

											<h4>
												<a href="{{ $result->url  }}">{{ $result->title }}</a>
											</h4>
											@if(isset($result->body) && !empty($result->body))
												<p>{!! substr(strip_tags($result->body),0,150) !!} ...</p>
											@elseif(isset($result->lead) && !empty($result->lead))
												<p>{!! substr(strip_tags($result->lead),0,150) !!} ...</p>
											@endif
											<a href="{{ $result->url  }}">lees meer</a>

										</div>
									</div>

								@endforeach

								<hr>

							@endif

						@endforeach

					</div>
				</div>

				@include('larawidget', ['hook' => 'content_bottom'])

			</div>

			{{-- Sidebar Right --}}
			@includeWhen($data->grid->hasSidebarRight, 'content._sidebars.right_default')

		</div>

	</div>
</section>