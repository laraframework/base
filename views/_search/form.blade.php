@extends('layout')

@section('content')

	<section class="module">
		<div class="container">

			<div class="row">
				<div class="col-sm-8 col-sm-offset-2">

					<h1>Zoeken</h1>

					{{ html()->form('GET', route('special.search.result'))
						->attributes(['accept-charset' => 'UTF-8'])
						->open() }}

					<div class="row form-group">
						<div class="col-sm-12 col-md-2">
							{{ html()->label('Search :', 'keywords') }}
						</div>
						<div class="col-sm-12 col-md-10">
							{{ html()->text('keywords', null)->class('form-control') }}
						</div>
					</div>

					{{ html()->button('Go', 'submit')->class('btn btn-danger btn-flat pull-right save-button') }}

					{{ html()->form()->close() }}


				</div>
			</div>
		</div>
	</section>


@endsection

