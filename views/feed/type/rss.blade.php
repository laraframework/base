<?php
if($entity->hasTags()) {
	$prefix = 'entitytag';
} else {
	$prefix = 'entity';
}
echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL
?>
<rss version="2.0">
	<channel>
		<title><![CDATA[{{ $data->meta->title }}]]></title>
		<link><![CDATA[{{ url($data->meta->link) }}]]></link>
		<description><![CDATA[{{ $data->meta->description }}]]></description>
		<language>{{ $data->meta->language }}</language>
		<pubDate>{{ $data->meta->updated->toRssString() }}</pubDate>
		@foreach($data->objects as $item)
			<item>
				<title><![CDATA[{{ $item->title }}]]></title>
				<link>{{ route($prefix . '.' . $entity->getEntityKey() . '.index.show', $item->slug) }}</link>
				<description><![CDATA[{!! $item->lead !!}]]></description>
				<author><![CDATA[{{ $item->user->email }} ({{ $item->user->name }})]]></author>
				<guid>{{ route($prefix . '.' . $entity->entity_key . '.index.show', $item->slug) }}</guid>
				<pubDate>{{ $item->updated_at->toRssString() }}</pubDate>
			</item>
		@endforeach
	</channel>
</rss>
