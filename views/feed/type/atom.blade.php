<?php
if($entity->hasTags()) {
	$prefix = 'entitytag';
} else {
	$prefix = 'entity';
}
echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL
?>
<feed xmlns="http://www.w3.org/2005/Atom">
	<title>{{ $data->meta->title }}</title>
	<link href="{{ url($data->meta->link) }}"/>
	<updated>{{ $data->meta->updated->toRfc3339String() }}</updated>
	<id>{{ url('/') }}/</id>

	@foreach($data->objects as $item)
		<entry>
			<title><![CDATA[{{ $item->title }}]]></title>
			<link rel="alternate" href="{{ route($prefix . '.' . $entity->getEntityKey() . '.index.show', $item->slug) }}"/>
			<id>{{ route($prefix . '.' . $entity->getEntityKey() . '.index.show', $item->slug) }}</id>
			<author>
				<name> <![CDATA[{{ $item->user->name }}]]></name>
				<email>{{ $item->user->email }}</email>
			</author>
			<summary type="html">
				<![CDATA[{!! $item->lead !!}]]>
			</summary>
			<updated>{{ $item->updated_at->toRfc3339String() }}</updated>
		</entry>
	@endforeach
</feed>