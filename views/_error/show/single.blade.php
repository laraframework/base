@include('larawidget', ['hook' => 'content_top'])

<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">

		{{-- Sidebar Left --}}
		@includeWhen($data->grid->hasSidebarLeft, 'content._sidebars.left_default')

		<div class="{{ $data->grid->contentCols }} main-content">

			<div class="row">
				<div class="{{ $data->grid->gridColumns }}">
					<h1 class="lara-object-title">
						{{ $data->object->title }}
					</h1>

					{{-- BODY TEXT --}}
					{!! $data->object->body !!}

				</div>
			</div>

			{{-- Sidebar Right --}}
			@includeWhen($data->grid->hasSidebarRight, 'content._sidebars.right_default')

		</div>

	</div>
</section>

@include('larawidget', ['hook' => 'content_bottom'])