<?php
// set default values for animations variables
$animation = new stdClass();
$animation->status = isset($animate) ? $animate : false;
$animation->class = isset($aclass) ? $aclass : config('lara-front.animation.class');
$animation->offset = isset($aoffset) ? $aoffset : config('lara-front.animation.offset');
$animation->duration = isset($aduration) ? $aduration : config('lara-front.animation.duration');
$animation->delay = isset($adelay) ? $adelay : config('lara-front.animation.delay');
$animation->easing = isset($aeasing) ? $aeasing : config('lara-front.animation.easing');

$widgetVars = [
	'grid'      => $data->grid,
	'animation' => $animation,
];

?>

{{-- Global widdgets --}}
@foreach($globalwidgets as $larawidget)
	@if($larawidget->hook == $hook)

		<?php $widgetVars['widget_id'] = $larawidget->id; ?>

		@if($larawidget->type == 'module')
			@if($larawidget->usecache)
				@widget('laraEntityCacheWidget', ['widget_id' => $larawidget->id, 'grid' => $data->grid])
			@else
				@widget('laraEntityWidget', ['widget_id' => $larawidget->id, 'grid' => $data->grid])
			@endif
		@else
			@widget('laraTextWidget', $widgetVars)
		@endif

	@endif
@endforeach

@if($entity->getEgroup() == 'page')

	{{-- single pages --}}
	@if(isset($data->object))
		@foreach($data->object->widgets as $larawidget)
			@if($larawidget->hook == $hook)

				<?php $widgetVars['widget_id'] = $larawidget->id; ?>

				@if($larawidget->type == 'module')
					@if($larawidget->usecache)
						@widget('laraEntityCacheWidget', ['widget_id' => $larawidget->id, 'grid' => $data->grid])
					@else
						@widget('laraEntityWidget', ['widget_id' => $larawidget->id, 'grid' => $data->grid])
					@endif
				@else
					@widget('laraTextWidget', $widgetVars)
				@endif

			@endif
		@endforeach
	@endif

@else

	{{-- entity page with associated block --}}
	@if(isset($data->page))
		@if(isset($data->page->widgets))
			@foreach($data->page->widgets as $larawidget)
				@if($larawidget->hook == $hook)

					<?php $widgetVars['widget_id'] = $larawidget->id; ?>

					@if($larawidget->type == 'module')
						@if($larawidget->usecache)
							@widget('laraEntityCacheWidget', $widgetVars)
						@else
							@widget('laraEntityWidget', $widgetVars)
						@endif
					@else
						@widget('laraTextWidget', $widgetVars)
					@endif

				@endif
			@endforeach
		@endif
	@endif

@endif










