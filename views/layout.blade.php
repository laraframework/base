<!DOCTYPE html>
<html lang="{{ $language }}" class="notranslate" translate="no">

<head>

	<meta name="google" content="notranslate" />

	@yield('head-before')

	@include('_main.html_header')

	@yield('head-after')

</head>

<body class="lara-{{ $entity->getEntityKey() }} lara-{{ $entity->getMethod() }} lara-{{ $data->params->vtype }}">

@include('_partials._google.gtm2')

<div class="wrapper">

	@include('_partials.header.'.$data->layout->header)

	@includeWhen($data->layout->hero, '_partials.hero.'.$data->layout->hero)

	@includeWhen($data->layout->pagetitle, '_partials.pagetitle.'.$data->layout->pagetitle)

	@includeWhen(isset($data->page), '_partials.pagehero.pagehero')

	@yield('content')

	@if(!$data->params->infinite)
		@includeWhen($data->layout->share, '_partials.sharing.'.$data->layout->share)
		@includeWhen($data->layout->cta, '_partials.cta.'.$data->layout->cta)
		@include('_partials.footer.'.$data->layout->footer)
	@endif

</div>
<!-- END WRAPPER -->

@yield('scripts-before')

@include('_main.html_footer')

@yield('scripts-after')

</body>
</html>

