@extends('layout_auth')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
				<div class="panel panel-default text-left">
					<div class="panel-heading text-center">
						<div class="row">
							<div class="col-xs-3">
								{!! Theme::img('images/logo-icon@2x.png', 'alt', 'pull-left', ['width' => '48']) !!}
							</div>
							<div class="col-xs-6 text-center">
								<h1>{{ ucfirst(_lanq('lara-front::user.headers.login')) }}</h1>
							</div>
						</div>
					</div>
					<div class="panel-body">
						@if (session('status'))
							<div class="alert alert-success">
								{{ session('status') }}
							</div>
						@endif

						<form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
							{{ csrf_field() }}

							<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
								<label for="email" class="col-md-4 control-label">
									{{ _lanq('lara-admin::user.column.email') }}
								</label>
								<div class="col-md-6">
									<input id="email" type="email" class="form-control" name="email"
									       value="{{ old('email') }}" required>

									@if ($errors->has('email'))
										<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
									@endif
								</div>
							</div>

							<div class="form-group">
								<div class="col-md-6 col-md-offset-4">
									<button type="submit" class="btn btn-primary">
										{{ _lanq('lara-front::user.button.send_password_reset_link') }}
									</button>
								</div>
							</div>
						</form>

						<hr>
						<div class="row">
							<div class="col-sm-12 text-right">
								<a href="{{ route('login') }}">
									{{ _lanq('lara-front::user.button.back_to_login') }}
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
