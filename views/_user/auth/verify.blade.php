@extends('layout_auth')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
            <div class="panel panel-default text-left">
                <div class="panel-heading">
	                {{ _lanq('lara-admin::user.headers.verify') }}
                </div>
                <div class="panel-body">
                    TEST
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
