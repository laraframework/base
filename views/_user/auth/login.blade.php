@extends('layout_auth')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4">
				<div class="panel panel-default text-left">
					<div class="panel-heading text-center">
						<div class="row">
							<div class="col-xs-3">
								{!! Theme::img('images/logo-icon@2x.png', 'alt', 'pull-left', ['width' => '48']) !!}
							</div>
							<div class="col-xs-6 text-center">
								<h1>{{ ucfirst(_lanq('lara-front::user.headers.login')) }}</h1>
							</div>
						</div>
					</div>
					<div class="panel-body">

						@if ($errors->any())
							<div class="alert alert-danger">
								<ul class="">
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@else
							@include('flash::message')
						@endif

						<form role="form" method="POST" action="{{ route('login') }}">
							{!! csrf_field()  !!}

							<input type="hidden" name="_login_type" value="frontend">

							<div class="form-group has-feedback">
								<input id="email" type="text" class="form-control" name="email"
								       value="{{ old('email') }}"
								       placeholder="{{ _lanq('lara-common::auth.loginform.placeholder_email') }}"
								       required
								       autofocus>
								<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
							</div>
							<div class="form-group has-feedback m-b-5">
								<input id="password" type="password" class="form-control" name="password"
								       placeholder="{{ _lanq('lara-common::auth.loginform.placeholder_password') }}"
								       required>
								<span class="glyphicon glyphicon-lock form-control-feedback"></span>
							</div>

							@if(config('lara.auth.can_register'))
								<div class="row m-b-20">
									<div class="col-xs-12 text-right">
										<a href="{{ route('password.request') }}" class="font-12">
											<u>{{ _lanq('lara-admin::user.button.forgot_password') }}</u>
										</a>
									</div>
								</div>
							@endif

							<div class="row m-b-20">
								<div class="col-xs-12">
									<button type="submit" class="btn btn-primary btn-block btn-flat login-button">
										{{ _lanq('lara-admin::user.button.login') }}
									</button>
								</div>
							</div>
							@if(config('lara.auth.can_register'))
								<hr>
								<div class="row m-t-20">
									<div class="col-xs-12">
										<p class="m-b-5 font-12">Nog geen account?</p>
										<a href="{{ route('register') }}" class="btn btn-flat btn-primary">
											{{ _lanq('lara-admin::user.button.register') }}
										</a>
									</div>
								</div>
							@endif
						</form>

					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('scripts-after')

	<script type="text/javascript">

		$(document).ready(function () {

			// spinner for save button
			$(".login-button").click(function () {
				$("button.login-button").html('<i class="fa fa-spin fa-circle-o-notch"></i>');
			});

		});

	</script>

@endsection
