@extends('layout_auth')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3">
				<div class="panel panel-default text-left">
					<div class="panel-heading text-center">
						<div class="row">
							<div class="col-xs-3">
								{!! Theme::img('images/logo-icon@2x.png', 'alt', 'pull-left', ['width' => '48']) !!}
							</div>
							<div class="col-xs-6 text-center">
								<h1>{{ ucfirst(_lanq('lara-front::user.headers.register')) }}</h1>
							</div>
						</div>
					</div>
					<div class="panel-body">
						@if(config('lara.auth.can_register'))
							<form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
								{{ csrf_field() }}

								<div class="form-group{{ $errors->has('firstname') ? ' has-error' : '' }}">
									<label for="firstname" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.firstname') }}
									</label>

									<div class="col-md-6">
										<input id="firstname" type="text" class="form-control" name="firstname"
										       value="{{ old('firstname') }}" required autofocus>

										@if ($errors->has('firstname'))
											<span class="help-block">
                                        <strong>{{ $errors->first('firstname') }}</strong>
                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('middlename') ? ' has-error' : '' }}">
									<label for="middlename" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.middlename') }}
									</label>

									<div class="col-md-6">
										<input id="middlename" type="text" class="form-control" name="middlename"
										       value="{{ old('middlename') }}" autofocus>

										@if ($errors->has('middlename'))
											<span class="help-block">
                                        <strong>{{ $errors->first('middlename') }}</strong>
                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('lastname') ? ' has-error' : '' }}">
									<label for="lastname" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.lastname') }}
									</label>

									<div class="col-md-6">
										<input id="lastname" type="text" class="form-control" name="lastname"
										       value="{{ old('lastname') }}" required autofocus>

										@if ($errors->has('lastname'))
											<span class="help-block">
                                        <strong>{{ $errors->first('lastname') }}</strong>
                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
									<label for="username" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.username') }}
									</label>
									<div class="col-md-6">
										<input id="username" type="text" class="form-control" name="username"
										       value="{{ old('username') }}" required>
										@if ($errors->has('username'))
											<span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
									<label for="email" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.email') }}
									</label>

									<div class="col-md-6">
										<input id="email" type="email" class="form-control" name="email"
										       value="{{ old('email') }}" required>

										@if ($errors->has('email'))
											<span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
									<label for="password" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.password') }}
									</label>

									<div class="col-md-6">
										<input id="password" type="password" class="form-control" name="password"
										       required>

										@if ($errors->has('password'))
											<span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
										@endif
									</div>
								</div>

								<div class="form-group">
									<label for="password-confirm" class="col-md-4 control-label">
										{{ _lanq('lara-admin::user.column.password_confirm') }}
									</label>

									<div class="col-md-6">
										<input id="password-confirm" type="password" class="form-control"
										       name="password_confirmation" required>
									</div>
								</div>

								<div class="form-group">
									<div class="col-md-6 col-md-offset-4">
										<button type="submit" class="btn btn-primary">
											{{ _lanq('lara-front::user.button.register') }}
										</button>
									</div>
								</div>
							</form>
							<hr>
							<div class="row">
								<div class="col-sm-12 text-right">
									<a href="{{ route('login') }}">
										{{ _lanq('lara-front::user.button.back_to_login') }}
									</a>
								</div>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
