@if($type == 'menu')

	<div class="btn-group lara-user-menu">
		@if (Auth::check())
			<button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true"
			        aria-expanded="false">
				<i class="fa fa-lg fa-user" aria-hidden="true"></i>
				<span class="hidden-xs hidden-sm">{{ Auth::user()->username }}</span> <span class="caret"></span>

			</button>
			<ul class="dropdown-menu dropdown-menu-right">
				<li>
					<a href="#">{{ Auth::user()->name }}</a>
				</li>
				<li role="separator" class="divider"></li>
				<li>
					<a href="{{ route('special.user.profile') }}">
						{{ _lanq('lara-front::user.menu.profile') }}
					</a>
				</li>
				@if(Auth::user()->hasLevel(90))
					<li>
						<a href="{{ route('admin.dashboard.index') }}" target="_blank">
							{{ _lanq('lara-front::user.menu.dashboard') }}
						</a>
					</li>
				@endif
				<li>
					<a href="{{ route('logout') }}"
					   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
						{{ _lanq('lara-front::user.menu.logouttext') }}
					</a>
				</li>
			</ul>
		@else
			<a href="{{ route('login', ['returnto' => $returnto]) }}" class="btn btn-logout">
				<i class="fa fa-lg fa-user" aria-hidden="true"></i>
				<span class="hidden-xs hidden-sm">{{ _lanq('lara-front::default.button.logintext') }}</span>
			</a>
		@endif
	</div>

@elseif($type == 'link')

	@if (Auth::check())
		<a href="{{ route('logout') }}" class="topbar-social-icon"
		   onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
			{{ _lanq('lara-front::default.button.logouttext') }}
		</a>
	@else
		<a href="{{ route('login', ['returnto' => $returnto]) }}" class="topbar-social-icon">
			{{ _lanq('lara-front::default.button.logintext') }}
		</a>
	@endif


@elseif($type == 'user')

	@if (Auth::check())
		{{ Auth::user()->name }}
	@endif

@endif