<section class="{{ $data->grid->module }}">
	<div class="{{ $data->grid->container }}">

		<div class="row">

			{{-- Sidebar Left --}}
			@includeWhen($data->grid->hasSidebarLeft, 'content._sidebars.left_default')

			<div class="{{ $data->grid->contentCols }} main-content">

				<div class="row">
					<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
						<h1 class="lara-object-title">{{ $data->page->title }}</h1>
						{!! $data->page->body !!}
					</div>
				</div>

				<div class="row">
					<div class="col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2">
						@include('_user.profile.show.form.form')
					</div>
				</div>

			</div>

			{{-- Sidebar Right --}}
			@includeWhen($data->grid->hasSidebarRight, 'content._sidebars.right_default')

		</div>

	</div>
</section>
