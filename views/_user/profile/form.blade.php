@extends('layout')

@section('head-after')
	{!! Theme::css('js/vendor/iCheck/lara/blue.css') !!}
	{!! Theme::css('js/vendor/iCheck/lara/aero.css') !!}
	{!! Theme::css('js/vendor/datepicker/bootstrap-datetimepicker.css') !!}
	<script src='https://www.google.com/recaptcha/api.js'></script>
@endsection

@section('content')

	@include('_user.profile.show.show')

@endsection


@section('scripts-after')

	<!-- iCheck -->
	{!! Theme::js('js/vendor/iCheck/icheck.min.js') !!}

	<script>
		$(document).ready(function () {
			// iCheck
			$('input[type=checkbox].icheckjs').iCheck({
				checkboxClass: 'icheckbox_lara-blue',
				radioClass: 'iradio_lara-blue'
			});

		});
	</script>



@endsection