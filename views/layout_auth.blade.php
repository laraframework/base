<!DOCTYPE html>
<html>
<head>

	@yield('head-before')

	@include('_main.html_header_auth')

	@yield('head-after')

</head>

<body class="hold-transition login-page">

@if(config('lara.has_frontend'))
<div class="ion-icon-to-front">
	<a href="{{ route('special.home.show') }}"
	   class="ion-icon-login-page">
		<i class="ion-ios-home ion-icon-login-page"></i>
	</a>
</div>
@endif

<div class="login-custom-bg">
	<div class="login-color-bg">

		<div class="login-wrapper">
			<div class="login-inner">

				@yield('content')

			</div>
		</div>

	</div>
</div>

@yield('scripts-before')

@include('_main.html_footer_auth')

@yield('scripts-after')

</body>
</html>
